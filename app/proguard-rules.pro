# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-optimizationpasses 5

#dagger
-dontwarn org.apache.**
-dontwarn dagger.android.**

#other
-dontwarn org.apache.lang.**
-dontwarn net.bytebuddy.**
-dontwarn java.lang.invoke.*
-dontwarn com.viewpagerindicator.**

# GSON
-keepattributes Signature
-keepattributes *Annotation*
-keepattributes EnclosingMethod
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
-keepclassmembers enum * { *; }

# UA
## Parcelable
-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}

## Required even for GCM only apps
-dontwarn com.amazon.device.messaging.**

# Retrofit2
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

# OkHttp
-dontwarn okhttp3.**
-dontwarn okio.**

# Picasso
-dontwarn com.squareup.okhttp.**

-dontwarn android.support.v4.**


########

-dontwarn sun.misc.Unsafe
-dontwarn javax.annotation.**

-keep class retrofit2.** { *; }
-keep class okhttp3.** { *; }
-keep class com.google.** { *; }
-keep class okio.** { *; }
-keep class io.reactivex.** { *; }
-keep class org.reactivestreams.** { *; }
-keep class com.twitter.** { *; }
-keep class com.squareup.** { *; }
-keep class com.ibm.** { *; }
-keep class bolts.** { *; }
-keep class javax.** { *; }
-keep class dagger.** { *; }
-keep class com.bumptech.** { *; }
-keep class com.vladsch.** { *; }
-keep class microsoft.** { *; }
-keep class org.java_websocket.** { *; }
-keep class org.apache.** { *; }
-keep class com.discretix.** { *; }
-keep class br.tiagohm.** { *; }
-keep class com.visualon.** { *; }
-keep class android.** {  *; }
-keep class com.mdialog.android.** {*;}

-dontwarn com.mdialog.**
-dontwarn com.visualon.**

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}

##app classes
-keep class learning.android.yogi.com.demo.model.** { *; }