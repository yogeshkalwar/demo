package learning.android.yogi.com.demo.countrysearch;

import io.reactivex.Observable;
import io.reactivex.Single;
import learning.android.yogi.com.demo.R;
import learning.android.yogi.com.demo.RxImmediateSchedulerRule;
import learning.android.yogi.com.demo.localstorage.LocalStorage;
import learning.android.yogi.com.demo.model.Country;
import learning.android.yogi.com.demo.navigation.Navigator;
import learning.android.yogi.com.demo.network.Api;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 02/12/18.
 */
public class CountrySearchScreenPresenterTest {

    @ClassRule
    public static final RxImmediateSchedulerRule schedulers = new RxImmediateSchedulerRule();

    private CountrySearchScreenPresenter mPresenter;
    private CountrySearchScreenContract.View mView;
    @Mock
    private Navigator mNavigator;
    @Mock
    private Api mApi;
    @Mock
    private LocalStorage mLocalStorage;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mView = Mockito.spy(CountrySearchScreenContract.View.class);
        mPresenter = new CountrySearchScreenPresenter(mView, mNavigator, mApi, mLocalStorage);
        Mockito.doNothing().when(mView).setPresenter(any());
    }

    @Test
    public void showCountry_Default_WhenOffline_One_storedOne() {
        final List<Country> singleCountry = getSingleCountryToLocalStorage();
        when(mLocalStorage.getAllCountries()).thenReturn(singleCountry);
        when(mView.isNetworkAvailable()).thenReturn(false);
        mPresenter.onInit();
        verify(mView).showUI(singleCountry);
    }

    @Test
    public void showCountry_Default_WhenOnline_None_storedOne() {
        when(mView.getSearchTextObservable()).thenReturn(Observable.just(""));
        when(mView.isNetworkAvailable()).thenReturn(true);
        mPresenter.onInit();
        verify(mView).showUI(new ArrayList<>());
    }

    @Test
    public void showCountry_WhenOnline_Search_India() {
        final List<Country> result = getResultForIndia();
        when(mView.getSearchTextObservable()).thenReturn(Observable.just("india"));
        when(mView.isNetworkAvailable()).thenReturn(true);
        when(mApi.getCountries("india")).thenReturn(Single.just(result));
        final CountrySearchScreenPresenter presenter = spy(mPresenter);
        doNothing().when(presenter).validateAndInitDisposable();
        presenter.onInit();
        verify(mView).showUI(result);
    }

    @Test
    public void showError_WhenOnline_Failed() {
        when(mView.getSearchTextObservable()).thenReturn(Observable.just("india"));
        when(mView.isNetworkAvailable()).thenReturn(true);
        when(mApi.getCountries(any())).thenReturn(Single.error(new Exception("general")));
        final CountrySearchScreenPresenter presenter = spy(mPresenter);
        doNothing().when(presenter).validateAndInitDisposable();
        presenter.onInit();
        verify(presenter).validateAndInitDisposable();
        verify(mView).showError(R.string.app_error_generic);
    }

    private List<Country> getSingleCountryToLocalStorage() {
        final ArrayList<Country> singleCountry = new ArrayList<>();
        final Country india = new Country();
        india.setCountry("India");
        singleCountry.add(india);
        return singleCountry;
    }

    private List<Country> getResultForIndia() {
        final ArrayList<Country> singleCountry = new ArrayList<>();
        singleCountry.add((new Country()).setCountry("British Indian Ocean Territory"));
        singleCountry.add((new Country()).setCountry("India"));
        return singleCountry;
    }
}
