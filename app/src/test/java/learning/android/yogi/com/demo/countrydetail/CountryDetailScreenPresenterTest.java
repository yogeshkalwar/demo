package learning.android.yogi.com.demo.countrydetail;

import learning.android.yogi.com.demo.localstorage.LocalStorage;
import learning.android.yogi.com.demo.model.Country;
import learning.android.yogi.com.demo.navigation.Navigator;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 02/12/18.
 */
public class CountryDetailScreenPresenterTest {

    private CountryDetailScreenPresenter mPresenter;
    @Mock
    private Navigator mNavigator;
    @Mock
    private LocalStorage mLocalStorage;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        final CountryDetailScreenContract.View view = Mockito.spy(CountryDetailScreenContract.View.class);
        final Country country = Mockito.spy(Country.class);
        Mockito.doNothing().when(view).setPresenter(any());
        mPresenter = new CountryDetailScreenPresenter(view, mNavigator, country, mLocalStorage);
    }
}
