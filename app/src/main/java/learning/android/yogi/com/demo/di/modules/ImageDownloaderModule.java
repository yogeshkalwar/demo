package learning.android.yogi.com.demo.di.modules;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import learning.android.yogi.com.demo.base.DemoApplication;
import learning.android.yogi.com.demo.network.image.GlideImageDownloader;
import learning.android.yogi.com.demo.network.image.ImageDownloader;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
@Module
public class ImageDownloaderModule {

    @Singleton
    @Provides
    static ImageDownloader provideImageDownloader(final DemoApplication application) {
        return new GlideImageDownloader(application);
    }
}
