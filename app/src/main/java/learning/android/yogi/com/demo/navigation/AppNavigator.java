package learning.android.yogi.com.demo.navigation;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import learning.android.yogi.com.demo.R;
import learning.android.yogi.com.demo.countrydetail.CountryDetailFragment;
import learning.android.yogi.com.demo.countrydetail.CountryDetailScreenContract;
import learning.android.yogi.com.demo.countrydetail.CountryDetailScreenPresenter;
import learning.android.yogi.com.demo.countrysearch.CountrySearchFragment;
import learning.android.yogi.com.demo.countrysearch.CountrySearchScreenContract.View;
import learning.android.yogi.com.demo.countrysearch.CountrySearchScreenPresenter;
import learning.android.yogi.com.demo.localstorage.LocalStorage;
import learning.android.yogi.com.demo.model.Country;
import learning.android.yogi.com.demo.network.Api;

import java.lang.ref.WeakReference;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class AppNavigator implements Navigator {

    protected final WeakReference<FragmentManager> mFragmentManagerWrapper;
    protected final WeakReference<Activity> mActivityWrapper;
    protected final Api mApi;
    protected final LocalStorage mLocalStorage;

    public AppNavigator(final FragmentManager fragmentManager, final Activity activity, final Api api, final
            LocalStorage localStorage) {
        mFragmentManagerWrapper = new WeakReference<>(fragmentManager);
        mActivityWrapper = new WeakReference<>(activity);
        mApi = api;
        mLocalStorage = localStorage;
    }

    @Override
    public void launchSearchScreen() {
        final Fragment fragment = CountrySearchFragment.instance();
        new CountrySearchScreenPresenter((View) fragment, this, mApi, mLocalStorage);
        showFragment(fragment, false);
    }

    @Override
    public void launchCountryDetailsScreen(final Country country) {
        final Fragment fragment = CountryDetailFragment.instance();
        new CountryDetailScreenPresenter((CountryDetailScreenContract.View) fragment, this, country, mLocalStorage);
        showFragment(fragment, true);
    }

    @Override
    public void closeCurrentScreen() {
        getFragmentManager().popBackStack();
    }

    private void showFragment(final Fragment fragment, final boolean addToBackStack) {
        final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_main_fragment_container, fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    private Activity getActivity() {
        Activity activity = null;
        if (mActivityWrapper != null) {
            activity = mActivityWrapper.get();
        }
        return activity;
    }

    private FragmentManager getFragmentManager() {
        FragmentManager fragmentManager = null;
        if (mFragmentManagerWrapper != null) {
            fragmentManager = mFragmentManagerWrapper.get();
        }
        return fragmentManager;
    }
}
