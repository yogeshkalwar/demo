package learning.android.yogi.com.demo.di.modules;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import learning.android.yogi.com.demo.base.DemoApplication;
import learning.android.yogi.com.demo.localstorage.LocalStorage;
import learning.android.yogi.com.demo.localstorage.PreferenceLocalStorage;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 30/11/18.
 */
@Module
public class LocalStorageModule {

    @Singleton
    @Provides
    static LocalStorage provideLocalStorage(final DemoApplication application) {
        return new PreferenceLocalStorage(application);
    }
}
