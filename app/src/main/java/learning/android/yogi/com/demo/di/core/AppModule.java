package learning.android.yogi.com.demo.di.core;

import android.app.Application;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import learning.android.yogi.com.demo.ui.MainActivity;
import learning.android.yogi.com.demo.di.modules.MainActivityModule;
import learning.android.yogi.com.demo.base.DemoApplication;
import learning.android.yogi.com.demo.di.scope.PerActivity;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
@Module(includes = AndroidSupportInjectionModule.class)
public abstract class AppModule {

    @Binds
    abstract Application application(DemoApplication app);

    @PerActivity
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mainActivityInjector();
}
