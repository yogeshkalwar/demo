package learning.android.yogi.com.demo.countrydetail;

import learning.android.yogi.com.demo.base.BasePresenter;
import learning.android.yogi.com.demo.base.BaseView;
import learning.android.yogi.com.demo.model.Country;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public interface CountryDetailScreenContract {

    interface View extends BaseView<Presenter> {

        void showUI(Country country);

        void hideSave();
    }

    interface Presenter extends BasePresenter {

        void save();
    }
}
