package learning.android.yogi.com.demo.localstorage;

import learning.android.yogi.com.demo.model.Country;

import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 30/11/18.
 */
public interface LocalStorage {

    void save(Country country);

    List<Country> getAllCountries();
}
