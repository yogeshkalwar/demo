package learning.android.yogi.com.demo.countrysearch;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import learning.android.yogi.com.demo.R;
import learning.android.yogi.com.demo.countrysearch.CountrySearchResultAdapter.CountryViewHolder;
import learning.android.yogi.com.demo.model.Country;
import learning.android.yogi.com.demo.network.image.ImageDownloader;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class CountrySearchResultAdapter extends RecyclerView.Adapter<CountryViewHolder> {

    private static final int VIEW_TYPE_COUNTRY = 0;

    private final ImageDownloader mImageDownloader;
    private final OnCountryClickListener mOnCountryClickListener;
    private final List<Country> mCountries;

    public CountrySearchResultAdapter(final OnCountryClickListener listener, final ImageDownloader imageDownloader) {
        mOnCountryClickListener = listener;
        mImageDownloader = imageDownloader;
        mCountries = new ArrayList<>();
    }

    public void setCountries(final List<Country> countries) {
        mCountries.clear();
        mCountries.addAll(countries);
        notifyDataSetChanged();
    }

    public void clear() {
        mCountries.clear();
        notifyDataSetChanged();
    }

    public String getCountryName(final int index) {
        String country = null;
        if (index < mCountries.size()) {
            country = mCountries.get(index).getCountryName();
        }
        return country;
    }

    @NonNull
    @Override
    public CountryViewHolder onCreateViewHolder(final @NonNull ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_country_row, parent, false);
        return new CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final @NonNull CountryViewHolder holder, final int position) {
        final Country country = mCountries.get(position);
        bindCountry(holder, country);
    }

    @Override
    public int getItemCount() {
        return mCountries.size();
    }

    @Override
    public int getItemViewType(final int position) {
        return VIEW_TYPE_COUNTRY;
    }

    private void bindCountry(final CountryViewHolder viewHolder, final Country country) {
        final String name = country.getCountryName();
        viewHolder.mCountry.setText(name);
        mImageDownloader.loadImage(country.getFlagUrl(), viewHolder.mFlag);
        viewHolder.itemView.setOnClickListener(v -> {
            if (mOnCountryClickListener != null) {
                mOnCountryClickListener.onCountryClicked(country);
            }
        });
    }

    static class CountryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.widget_country_row_country)
        public TextView mCountry;
        @BindView(R.id.widget_country_row_flag)
        public ImageView mFlag;

        public CountryViewHolder(final View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
