package learning.android.yogi.com.demo.di.modules;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import dagger.Module;
import dagger.Provides;
import javax.inject.Named;
import learning.android.yogi.com.demo.di.scope.PerFragment;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
@Module
public abstract class BaseFragmentModule {

    private static final String NAMED_FRAGMENT = "BaseFragmentModule.fragment";
    private static final String NAMED_CHILD_FRAGMENT_MANAGER = "BaseFragmentModule.mChildFragmentManager";

    @Provides
    @Named(NAMED_CHILD_FRAGMENT_MANAGER)
    @PerFragment
    static FragmentManager childFragmentManager(@Named(NAMED_FRAGMENT) final Fragment fragment) {
        return fragment.getChildFragmentManager();
    }
}
