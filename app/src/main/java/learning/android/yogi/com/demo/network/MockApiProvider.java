package learning.android.yogi.com.demo.network;

import android.content.Context;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class MockApiProvider extends BaseApiProvider{

    public MockApiProvider(Context context) {
        super(context);
    }

    @Override
    protected String getBaseUrl() {
        throw new UnsupportedOperationException();
    }
}
