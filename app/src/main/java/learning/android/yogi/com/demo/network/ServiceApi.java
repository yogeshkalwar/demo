package learning.android.yogi.com.demo.network;

import io.reactivex.Single;
import learning.android.yogi.com.demo.model.Country;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Interface for Services side api
 * Created on 28/11/18.
 */
public interface ServiceApi {

    @GET("name/{country}")
    Single<List<Country>> getCountries(@Path("country") String name);
}
