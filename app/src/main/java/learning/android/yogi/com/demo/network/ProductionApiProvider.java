package learning.android.yogi.com.demo.network;

import android.content.Context;

import learning.android.yogi.com.demo.R;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class ProductionApiProvider extends BaseApiProvider {

    public ProductionApiProvider(final Context context) {
        super(context);
    }

    @Override
    protected String getBaseUrl() {
        return getContext().getString(R.string.api_base_url);
    }
}
