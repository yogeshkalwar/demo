package learning.android.yogi.com.demo.di.modules;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import learning.android.yogi.com.demo.receivers.NetworkConnectivityManager;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 30/11/18.
 */
@Module
public class NetworkConnectivityModule {

    @Singleton
    @Provides
    static NetworkConnectivityManager provideConnectivityManager() {
        return new NetworkConnectivityManager();
    }
}
