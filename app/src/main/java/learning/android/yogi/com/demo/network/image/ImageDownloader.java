package learning.android.yogi.com.demo.network.image;

import android.widget.ImageView;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public interface ImageDownloader {

    interface OnImageDownloadListener {

        void onFailure();

        void onSuccess();
    }

    void loadImage(String url, ImageView imageView);

    void loadImage(String url, ImageView imageView, OnImageDownloadListener listener);
}
