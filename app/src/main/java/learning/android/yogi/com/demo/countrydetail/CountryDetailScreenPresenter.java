package learning.android.yogi.com.demo.countrydetail;

import learning.android.yogi.com.demo.localstorage.LocalStorage;
import learning.android.yogi.com.demo.model.Country;
import learning.android.yogi.com.demo.navigation.Navigator;

/**
 * Copyright (c) 2018 DigitasLBi. All rights reserved.
 * Created on 28/11/18.
 */
public class CountryDetailScreenPresenter implements CountryDetailScreenContract.Presenter {

    private final CountryDetailScreenContract.View mView;
    private final Navigator mNavigator;
    private final Country mCountry;
    private final LocalStorage mLocalStorage;

    public CountryDetailScreenPresenter(final CountryDetailScreenContract.View view, final Navigator navigator, final Country country, final
            LocalStorage localStorage) {
        mView = view;
        mNavigator = navigator;
        mCountry = country;
        mLocalStorage = localStorage;
        mView.setPresenter(this);
    }

    @Override
    public void onInit() {
        mView.showUI(mCountry);
        if (!mView.isNetworkAvailable()) {
            mView.hideSave();
        }
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public void save() {
        mLocalStorage.save(mCountry);
        mNavigator.closeCurrentScreen();
    }
}
