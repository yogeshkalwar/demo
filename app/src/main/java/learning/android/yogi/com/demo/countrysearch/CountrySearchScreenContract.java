package learning.android.yogi.com.demo.countrysearch;

import io.reactivex.Observable;
import learning.android.yogi.com.demo.base.BasePresenter;
import learning.android.yogi.com.demo.base.BaseView;
import learning.android.yogi.com.demo.model.Country;

import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public interface CountrySearchScreenContract {

    interface View extends BaseView<Presenter> {

        void showUI(List<Country> countries);

        Observable<String> getSearchTextObservable();

        void showError(int resource);

        String getCountryName(int index);
    }

    interface Presenter extends BasePresenter {

        void onCountryClicked(Country country);
    }
}
