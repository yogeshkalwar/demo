package learning.android.yogi.com.demo.di.modules;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import learning.android.yogi.com.demo.base.DemoApplication;
import learning.android.yogi.com.demo.network.Api;
import learning.android.yogi.com.demo.network.ProductionApiProvider;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
@Module
public class NetworkModule {

    @Singleton
    @Provides
    static Api provideApi(final DemoApplication application) {
        return new ProductionApiProvider(application);
    }
}
