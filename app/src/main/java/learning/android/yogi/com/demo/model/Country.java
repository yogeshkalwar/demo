package learning.android.yogi.com.demo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class Country {

    @SerializedName("name")
    private String mCountry;
    @SerializedName("flag")
    private String mFlagUrl;
    @SerializedName("capital")
    private String mCapital;
    @SerializedName("callingCodes")
    private List<String> mCallingCodes;
    @SerializedName("region")
    private String mRegion;
    @SerializedName("subregion")
    private String mSubRegion;
    @SerializedName("timezones")
    private List<String> mTimezones;
    @SerializedName("currencies")
    private List<Currency> mCurrencies;
    @SerializedName("languages")
    private List<Language> mLanguages;

    public String getCountryName() {
        return mCountry;
    }

    public String getFlagUrl() {
        return mFlagUrl;
    }

    public String getCapital() {
        return mCapital;
    }

    public String getCallingCodes() {
        final StringBuilder builder = new StringBuilder();
        for (final String code : mCallingCodes) {
            builder.append(code).append(",");
        }
        return builder.toString();
    }

    public String getRegion() {
        return mRegion;
    }

    public String getSubRegion() {
        return mSubRegion;
    }

    public String getTimezones() {
        final StringBuilder builder = new StringBuilder();
        for (final String zone : mTimezones) {
            builder.append(zone).append(",");
        }
        return builder.toString();
    }

    public String getCurrencies() {
        final StringBuilder builder = new StringBuilder();
        for (final Currency currency : mCurrencies) {
            builder.append(currency.toString()).append(",");
        }
        return builder.toString();
    }

    public String getLanguages() {
        final StringBuilder builder = new StringBuilder();
        for (final Language language : mLanguages) {
            builder.append(language.toString()).append(",");
        }
        return builder.toString();
    }

    public Country setCountry(final String name) {
        mCountry = name;
        return this;
    }
}
