package learning.android.yogi.com.demo.countrydetail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import javax.inject.Inject;
import learning.android.yogi.com.demo.R;
import learning.android.yogi.com.demo.model.Country;
import learning.android.yogi.com.demo.network.image.ImageDownloader;
import learning.android.yogi.com.demo.ui.BaseFragment;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class CountryDetailFragment extends BaseFragment<CountryDetailScreenContract.Presenter> implements CountryDetailScreenContract.View {

    public static CountryDetailFragment instance() {
        return new CountryDetailFragment();
    }

    @BindView(R.id.fragment_country_detail_screen_name)
    TextView mName;
    @BindView(R.id.fragment_country_detail_screen_flag)
    ImageView mFlag;
    @BindView(R.id.fragment_country_detail_screen_capital)
    TextView mCapital;
    @BindView(R.id.fragment_country_detail_screen_calling_codes)
    TextView mCallingCodes;
    @BindView(R.id.fragment_country_detail_screen_region)
    TextView mRegion;
    @BindView(R.id.fragment_country_detail_screen_subregion)
    TextView mSubregion;
    @BindView(R.id.fragment_country_detail_screen_timezone)
    TextView mTimezones;
    @BindView(R.id.fragment_country_detail_screen_currencies)
    TextView mCurrencies;
    @BindView(R.id.fragment_country_detail_screen_languages)
    TextView mLanguages;
    @BindView(R.id.fragment_country_detail_screen_save)
    Button mSave;

    @Inject
    ImageDownloader mImageDownloader;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_country_detail_screen, container, false);
        ButterKnife.bind(this, view);
        mBasePresenter.onInit();
        return view;
    }

    @Override
    public void showUI(final Country country) {
        mName.setText(country.getCountryName());
        mImageDownloader.loadImage(country.getFlagUrl(), mFlag);
        mCapital.setText(country.getCapital());
        mCallingCodes.setText(country.getCallingCodes());
        mRegion.setText(country.getRegion());
        mSubregion.setText(country.getSubRegion());
        mTimezones.setText(country.getTimezones());
        mCurrencies.setText(country.getCurrencies());
        mLanguages.setText(country.getLanguages());
    }

    @Override
    public void hideSave() {
        mSave.setVisibility(View.GONE);
    }

    @OnClick(R.id.fragment_country_detail_screen_save)
    void onSaveClicked() {
        ((CountryDetailScreenContract.Presenter) mBasePresenter).save();
    }
}
