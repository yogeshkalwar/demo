package learning.android.yogi.com.demo.countrysearch;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import javax.inject.Inject;
import learning.android.yogi.com.demo.R;
import learning.android.yogi.com.demo.model.Country;
import learning.android.yogi.com.demo.network.image.ImageDownloader;
import learning.android.yogi.com.demo.receivers.ConnectivityListener;
import learning.android.yogi.com.demo.receivers.NetworkConnectivityManager;
import learning.android.yogi.com.demo.ui.BaseFragment;

import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class CountrySearchFragment extends BaseFragment<CountrySearchScreenContract.Presenter> implements OnCountryClickListener, ConnectivityListener, CountrySearchScreenContract.View {

    public static CountrySearchFragment instance() {
        return new CountrySearchFragment();
    }

    @BindView(R.id.fragment_search_screen_results)
    RecyclerView mCountriesRecyclerView;
    @BindView(R.id.fragment_search_screen_search)
    EditText mSearchEditText;

    @Inject
    ImageDownloader mImageDownloader;
    @Inject
    NetworkConnectivityManager mNetworkConnectivityManager;

    private CountrySearchResultAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_search_screen, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    private void initUI() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mCountriesRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new CountrySearchResultAdapter(this, mImageDownloader);
        mCountriesRecyclerView.setHasFixedSize(true);
        mCountriesRecyclerView.setAdapter(mAdapter);
        mNetworkConnectivityManager.addListener(this);
        mBasePresenter.onInit();
    }

    @Override
    public void onCountryClicked(final Country country) {
        ((CountrySearchScreenPresenter)mBasePresenter).onCountryClicked(country);
    }

    @Override
    public void showUI(final List<Country> countries) {
        mAdapter.setCountries(countries);
    }

    @Override
    public Observable<String> getSearchTextObservable() {
        Observable<String> searchTextObservable = Observable.create(emitter -> {
            final TextWatcher watcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
                }

                @Override
                public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                    emitter.onNext(s.toString().trim());
                }

                @Override
                public void afterTextChanged(final Editable s) {

                }
            };
            mSearchEditText.addTextChangedListener(watcher);
            emitter.setCancellable(() -> mSearchEditText.removeTextChangedListener(watcher));
        });
        return searchTextObservable;
    }

    @Override
    public void showError(final int resource) {
        Toast.makeText(getContext(), getString(resource), Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getCountryName(final int index) {
        return mAdapter.getCountryName(index);
    }

    @Override
    public void onDestroyView() {
        mNetworkConnectivityManager.removeListener(this);
        mBasePresenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void onNetworkStateChanged(final boolean available) {
        mBasePresenter.onInit();
        if (available) {
            final String text = mSearchEditText.getText().toString();
            mAdapter.clear();
            mSearchEditText.setText(text);
            mSearchEditText.setSelection(text.length());
        }
    }
}
