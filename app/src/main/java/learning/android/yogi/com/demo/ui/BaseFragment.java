package learning.android.yogi.com.demo.ui;

import android.content.Context;
import android.support.v4.app.Fragment;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;
import javax.inject.Inject;
import learning.android.yogi.com.demo.base.BasePresenter;
import learning.android.yogi.com.demo.base.BaseView;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class BaseFragment<T extends BasePresenter> extends Fragment implements BaseView<T>, HasSupportFragmentInjector {

    protected BasePresenter mBasePresenter;
    protected ActivityInteractor mActivityInteractor;

    @Inject
    DispatchingAndroidInjector<Fragment> mChildFragmentInjector;

    @Override
    public void setPresenter(final T presenter) {
        mBasePresenter = presenter;
    }

    @Override
    public void showLoading() {
        if (mActivityInteractor != null) {
            mActivityInteractor.showLoading();
        }
    }

    @Override
    public void hideLoading() {
        if (mActivityInteractor != null) {
            mActivityInteractor.hideLoading();
        }
    }

    @Override
    public boolean isNetworkAvailable() {
        boolean available = true;
        if (mActivityInteractor != null) {
            available = mActivityInteractor.isNetworkAvailable();
        }
        return available;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mBasePresenter != null) {
            mBasePresenter.onResume();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof ActivityInteractor) {
            mActivityInteractor = (ActivityInteractor) getActivity();
        }
        AndroidSupportInjection.inject(this);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return mChildFragmentInjector;
    }
}
