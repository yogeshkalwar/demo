package learning.android.yogi.com.demo.base;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public interface BaseView<T> {

    void setPresenter(T presenter);

    void showLoading();

    void hideLoading();

    boolean isNetworkAvailable();
}
