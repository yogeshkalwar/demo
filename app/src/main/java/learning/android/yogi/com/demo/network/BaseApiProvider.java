package learning.android.yogi.com.demo.network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import io.reactivex.Single;
import learning.android.yogi.com.demo.BuildConfig;
import learning.android.yogi.com.demo.model.Country;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public abstract class BaseApiProvider implements Api {

    private final WeakReference<Context> mContextWrapper;
    private final ServiceApi mServiceApi;

    BaseApiProvider(final Context context) {
        mContextWrapper = new WeakReference<>(context);
        final Gson gson = getGson();
        //retrofit builder for custom gson converter, call adaptor if needed
        final Retrofit.Builder builder = new Builder().baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        //ok http client for custom cache, interceptor, retry policy, timeout if needed
        final OkHttpClient.Builder client = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(Level.BODY);
            client.addInterceptor(loggingInterceptor);
        }
        builder.client(client.build());
        mServiceApi = builder.build().create(ServiceApi.class);
    }

    private Gson getGson() {
        final GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Override
    public Single<List<Country>> getCountries(final String name) {
        return mServiceApi.getCountries(name);
    }

    protected abstract String getBaseUrl();

    protected Context getContext() {
        Context context = null;
        if (mContextWrapper != null) {
            context = mContextWrapper.get();
        }
        return context;
    }
}
