package learning.android.yogi.com.demo.countrysearch;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import learning.android.yogi.com.demo.R;
import learning.android.yogi.com.demo.localstorage.LocalStorage;
import learning.android.yogi.com.demo.model.Country;
import learning.android.yogi.com.demo.navigation.Navigator;
import learning.android.yogi.com.demo.network.Api;
import timber.log.Timber;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (c) 2018 DigitasLBi. All rights reserved.
 * Created on 28/11/18.
 */
public class CountrySearchScreenPresenter implements CountrySearchScreenContract.Presenter {

    private static final int MINIMUM_TEXT_LENGTH_FOR_SEARCH = 3;
    private static final int DELAY_FOR_TEXT_CHANGE = 300;

    private final CountrySearchScreenContract.View mView;
    private final Navigator mNavigator;
    private final Api mApi;
    private final LocalStorage mLocalStorage;

    private Disposable mDisposable;

    public CountrySearchScreenPresenter(final CountrySearchScreenContract.View view, final Navigator navigator, final Api api, final LocalStorage localStorage) {
        mView = view;
        mNavigator = navigator;
        mApi = api;
        mLocalStorage = localStorage;
        mView.setPresenter(this);
    }

    public void initSearchApiCall() {
        mDisposable = mView.getSearchTextObservable()
                .filter(s -> {
                    if (s.length() < MINIMUM_TEXT_LENGTH_FOR_SEARCH) {
                        loadCountries(new ArrayList<>());
                        return false;
                    }
                    return true;
                })
                .debounce(DELAY_FOR_TEXT_CHANGE, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(s -> mView.showLoading())
                .observeOn(Schedulers.io())
                .map(s -> mApi.getCountries(s).blockingGet())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(countries -> loadCountries(countries), this::showError);
    }

    @Override
    public void onInit() {
        if (mView.isNetworkAvailable()) {
            initSearchApiCall();
        } else {
            dispose();
            loadCountries(mLocalStorage.getAllCountries());
        }
    }

    @Override
    public void onResume() {
        validateAndInitDisposable();
    }

    public void validateAndInitDisposable() {
        if (mDisposable == null || mDisposable.isDisposed()) {
            onInit();
        }
    }

    @Override
    public void onDestroy() {
        dispose();
    }

    private void dispose() {
        if (mDisposable != null && !mDisposable.isDisposed()) {
            Timber.d("Demo: Disposed");
            mDisposable.dispose();
            mDisposable = null;
        }
    }

    private void loadCountries(final List<Country> countries) {
        mView.showUI(countries);
        mView.hideLoading();
    }

    private void showError(final Throwable error) {
        Timber.d(error);
        mView.showError(R.string.app_error_generic);
        mView.hideLoading();
        validateAndInitDisposable();
    }

    @Override
    public void onCountryClicked(final Country country) {
        mNavigator.launchCountryDetailsScreen(country);
    }
}
