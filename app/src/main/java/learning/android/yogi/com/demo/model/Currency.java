package learning.android.yogi.com.demo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 30/11/18.
 */
public class Currency {
    @SerializedName("code")
    private String mCode;
    @SerializedName("name")
    private String mName;
    @SerializedName("symbol")
    private String mSymbol;

    @Override
    public String toString() {
        return new StringBuilder("[code:").append(mCode).append(", name:").append(mName).append(", symbol:").append(mSymbol).append("]").toString();
    }
}
