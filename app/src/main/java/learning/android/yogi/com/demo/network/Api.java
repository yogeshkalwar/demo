package learning.android.yogi.com.demo.network;

import io.reactivex.Single;
import learning.android.yogi.com.demo.model.Country;

import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Interface for Api, to be used by app
 * Created on 28/11/18.
 */
public interface Api {

    Single<List<Country>> getCountries(String name);
}
