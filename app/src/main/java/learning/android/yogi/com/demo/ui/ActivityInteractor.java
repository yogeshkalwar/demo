package learning.android.yogi.com.demo.ui;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 29/11/18.
 */
public interface ActivityInteractor {

    void showLoading();

    void hideLoading();

    boolean isNetworkAvailable();
}
