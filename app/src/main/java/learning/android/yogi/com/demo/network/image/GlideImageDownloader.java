package learning.android.yogi.com.demo.network.image;

import android.annotation.TargetApi;
import android.graphics.Picture;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Build;
import android.widget.ImageView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.SimpleResource;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;
import learning.android.yogi.com.demo.R;
import learning.android.yogi.com.demo.base.DemoApplication;

import java.io.IOException;
import java.io.InputStream;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class GlideImageDownloader implements ImageDownloader {

    private final GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> mRequestBuilder;

    public GlideImageDownloader(final DemoApplication application) {
        mRequestBuilder = Glide.with(application)
                .using(Glide.buildStreamModelLoader(Uri.class, application), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.drawable.country_placeholder)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<>());
    }

    @Override
    public void loadImage(final String url, final ImageView imageView) {
        loadImage(url, imageView, null);
    }

    @Override
    public void loadImage(final String url, final ImageView imageView, final OnImageDownloadListener listener) {
        mRequestBuilder.diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .load(Uri.parse(url))
                .into(imageView);
    }

    static class SvgDecoder implements ResourceDecoder<InputStream, SVG> {
        public Resource<SVG> decode(InputStream source, int width, int height) throws IOException {
            try {
                SVG svg = SVG.getFromInputStream(source);
                return new SimpleResource<>(svg);
            } catch (SVGParseException ex) {
                throw new IOException("Cannot load SVG from stream", ex);
            }
        }

        @Override
        public String getId() {
            return "SvgDecoder.com.bumptech.svgsample.app";
        }
    }

    static class SvgDrawableTranscoder implements ResourceTranscoder<SVG, PictureDrawable> {
        @Override
        public Resource<PictureDrawable> transcode(Resource<SVG> toTranscode) {
            SVG svg = toTranscode.get();
            Picture picture = svg.renderToPicture();
            PictureDrawable drawable = new PictureDrawable(picture);
            return new SimpleResource<>(drawable);
        }

        @Override
        public String getId() {
            return "";
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    static class SvgSoftwareLayerSetter<T> implements RequestListener<T, PictureDrawable> {

        @Override
        public boolean onException(Exception e, T model, Target<PictureDrawable> target, boolean isFirstResource) {
            ImageView view = ((ImageViewTarget<?>) target).getView();
            if (Build.VERSION_CODES.HONEYCOMB <= Build.VERSION.SDK_INT) {
                view.setLayerType(ImageView.LAYER_TYPE_NONE, null);
            }
            return false;
        }

        @Override
        public boolean onResourceReady(PictureDrawable resource, T model, Target<PictureDrawable> target,
                boolean isFromMemoryCache, boolean isFirstResource) {
            ImageView view = ((ImageViewTarget<?>) target).getView();
            if (Build.VERSION_CODES.HONEYCOMB <= Build.VERSION.SDK_INT) {
                view.setLayerType(ImageView.LAYER_TYPE_SOFTWARE, null);
            }
            return false;
        }
    }
}
