package learning.android.yogi.com.demo.base;

import android.app.Activity;
import android.app.Application;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import javax.inject.Inject;
import learning.android.yogi.com.demo.BuildConfig;
import learning.android.yogi.com.demo.di.core.DaggerAppComponent;
import learning.android.yogi.com.demo.receivers.NetworkConnectivityManager;
import timber.log.Timber;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class DemoApplication extends Application implements HasActivityInjector{

    @Inject
    DispatchingAndroidInjector<Activity> mActivityInjector;

    @Inject
    NetworkConnectivityManager mNetworkConnectivityManager;

    @Override
    public void onCreate() {
        super.onCreate();
        initDependencyInjection();
        initTimber();
        init();
    }

    private void init() {
        registerReceiver(mNetworkConnectivityManager, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void initDependencyInjection() {
        DaggerAppComponent.builder().create(this).inject(this);
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    @Override
    public void onTerminate() {
        if (mNetworkConnectivityManager != null) {
            unregisterReceiver(mNetworkConnectivityManager);
        }
        super.onTerminate();
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return mActivityInjector;
    }
}
