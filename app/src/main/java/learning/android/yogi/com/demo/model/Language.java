package learning.android.yogi.com.demo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 30/11/18.
 */
public class Language {
    @SerializedName("iso639_1")
    private String mIso1;
    @SerializedName("iso639_2")
    private String mIso2;
    @SerializedName("name")
    private String mName;
    @SerializedName("nativeName")
    private String mNativeName;

    @Override
    public String toString() {
        return new StringBuilder("[iso639_1:").append(mIso1)
                .append(", iso639_2:").append(mIso2)
                .append(", name:").append(mName)
                .append(", nativeName:").append(mNativeName)
                .append("]").toString();
    }
}
