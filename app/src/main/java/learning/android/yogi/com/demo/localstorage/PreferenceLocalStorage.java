package learning.android.yogi.com.demo.localstorage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

import com.google.gson.Gson;
import learning.android.yogi.com.demo.model.Country;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 30/11/18.
 */
public class PreferenceLocalStorage implements LocalStorage {

    private static final String PREFERENCE_NAME = "localstorage_pref";
    private static final String KEY_NAMES = "localstorage_names";
    private static final String EMPTY = "";

    private final SharedPreferences mSharedPreferences;
    private final Gson mGson;

    public PreferenceLocalStorage(final Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, 0);
        mGson = new Gson();
        init();
    }

    private void init() {
    }

    @Override
    public void save(final Country country) {
        final Editor editor = mSharedPreferences.edit();

        final String name = country.getCountryName();
        editor.putString(name, mGson.toJson(country));
        final Set<String> names = mSharedPreferences.getStringSet(KEY_NAMES, new HashSet<>());
        names.add(name);
        editor.putStringSet(KEY_NAMES, names);
        editor.apply();
    }

    @Override
    public List<Country> getAllCountries() {
        final Set<String> keys = mSharedPreferences.getStringSet(KEY_NAMES, null);
        final List<Country> countries = new ArrayList<>();
        if (keys != null) {
            for (final String key : keys) {
                final String json = mSharedPreferences.getString(key, EMPTY).trim();
                if (!TextUtils.isEmpty(json)) {
                    countries.add(mGson.fromJson(json, Country.class));
                }
            }
        }
        return countries;
    }
}
