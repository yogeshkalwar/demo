package learning.android.yogi.com.demo.model;

import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public class CountriesResponse {
    List<Country> mCountries;

    public List<Country> getCountries() {
        return mCountries;
    }
}
