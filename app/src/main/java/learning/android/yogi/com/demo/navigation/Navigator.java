package learning.android.yogi.com.demo.navigation;

import learning.android.yogi.com.demo.model.Country;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
public interface Navigator {

    void launchSearchScreen();

    void launchCountryDetailsScreen(Country country);

    void closeCurrentScreen();
}
