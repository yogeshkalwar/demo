package learning.android.yogi.com.demo.receivers;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 30/11/18.
 */
public interface ConnectivityListener {

    void onNetworkStateChanged(boolean available);
}
