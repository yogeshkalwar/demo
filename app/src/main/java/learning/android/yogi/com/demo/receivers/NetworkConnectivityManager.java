package learning.android.yogi.com.demo.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 30/11/18.
 */
public class NetworkConnectivityManager extends BroadcastReceiver {

    private final List<ConnectivityListener> mConnectivityListeners;

    public NetworkConnectivityManager() {
        mConnectivityListeners = new ArrayList<>();
    }

    public void addListener(final ConnectivityListener listener) {
        mConnectivityListeners.add(listener);
    }

    public void removeListener(final ConnectivityListener listener) {
        mConnectivityListeners.remove(listener);
    }

    public void broadcastState(final boolean state) {
        for (final ConnectivityListener listener : mConnectivityListeners) {
            if (listener != null) {
                listener.onNetworkStateChanged(state);
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        broadcastState(isConnectedToNetwork(context));
    }

    private static boolean isConnectedToNetwork(final Context context) {
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
