package learning.android.yogi.com.demo.di.core;

import dagger.Component;
import dagger.android.AndroidInjector;
import javax.inject.Singleton;
import learning.android.yogi.com.demo.base.DemoApplication;
import learning.android.yogi.com.demo.di.modules.ImageDownloaderModule;
import learning.android.yogi.com.demo.di.modules.LocalStorageModule;
import learning.android.yogi.com.demo.di.modules.NetworkConnectivityModule;
import learning.android.yogi.com.demo.di.modules.NetworkModule;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, ImageDownloaderModule.class, LocalStorageModule.class,
        NetworkConnectivityModule.class})
interface AppComponent extends AndroidInjector<DemoApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<DemoApplication> {
    }
}
