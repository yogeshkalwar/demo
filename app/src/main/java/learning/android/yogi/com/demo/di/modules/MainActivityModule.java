package learning.android.yogi.com.demo.di.modules;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import learning.android.yogi.com.demo.countrydetail.CountryDetailFragment;
import learning.android.yogi.com.demo.di.scope.PerFragment;
import learning.android.yogi.com.demo.localstorage.LocalStorage;
import learning.android.yogi.com.demo.navigation.AppNavigator;
import learning.android.yogi.com.demo.navigation.Navigator;
import learning.android.yogi.com.demo.countrysearch.CountrySearchFragment;
import learning.android.yogi.com.demo.network.Api;
import learning.android.yogi.com.demo.ui.MainActivity;
import learning.android.yogi.com.demo.di.scope.PerActivity;

/**
 * Copyright (c) 2018 Yogesh Kalwar. All rights reserved.
 * Created on 28/11/18.
 */
@Module
public abstract class MainActivityModule {

    @Binds
    @PerActivity
    abstract Activity activity(final MainActivity mainActivity);

    @Binds
    @PerActivity
    abstract Context activityContext(final Activity activity);

    @Provides
    @PerActivity
    static FragmentManager activityFragmentManager(final Activity activity) {
        return ((AppCompatActivity) activity).getSupportFragmentManager();
    }

    @Provides
    @PerActivity
    static Navigator provideNavigator(final FragmentManager fragmentManager, final Activity activity, final Api api, final
            LocalStorage localStorage) {
        return new AppNavigator(fragmentManager, activity, api, localStorage);
    }

    @PerFragment
    @ContributesAndroidInjector(modules = BaseFragmentModule.class)
    abstract CountrySearchFragment searchFragmentInjector();

    @PerFragment
    @ContributesAndroidInjector(modules = BaseFragmentModule.class)
    abstract CountryDetailFragment countryDetailFragment();
}
